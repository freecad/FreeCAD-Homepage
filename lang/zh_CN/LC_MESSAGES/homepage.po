msgid ""
msgstr ""
"Project-Id-Version: freecad\n"
"POT-Creation-Date: 2017-07-18 11:51-0300\n"
"PO-Revision-Date: 2017-07-18 11:51-0300\n"
"Last-Translator: yorik <yorik.vanhavre@gmail.com>\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.2\n"
"X-Poedit-Basepath: ../../..\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: freecad\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: homepage.po\n"
"X-Poedit-SearchPath-0: index.php\n"

#: index.php:43
msgid "FreeCAD: An open-source parametric 3D CAD modeler"
msgstr "FreeCAD: 开放源参数化三维 CAD 建模器"

#: index.php:56
msgid "Toggle navigation"
msgstr "切换导航"

#: index.php:111
msgid "Forum"
msgstr "论坛"

#: index.php:112
msgid "Documentation"
msgstr "文档"

#: index.php:113
msgid "Bug tracker"
msgstr "Bug 追踪器"

#: index.php:124
msgid "Overview"
msgstr "简介"

#: index.php:125
msgid "Features"
msgstr "功能"

#: index.php:126
msgid "Screenshots"
msgstr "屏幕截图"

#: index.php:127
msgid "Download"
msgstr "下载"

#: index.php:128
msgid "Getting started"
msgstr "开始"

#: index.php:129
msgid "Users showcase"
msgstr "用户展示区"

#: index.php:130
msgid "Get help"
msgstr "获取帮助"

#: index.php:131
#, fuzzy
#| msgid "title=Help_FreeCAD"
msgid "Help FreeCAD"
msgstr "title=Help_FreeCAD/zh-cn"

#: index.php:137
msgid "Welcome!"
msgstr "欢迎 ！"

#: index.php:140
msgid ""
"FreeCAD is a parametric 3D modeler made primarily to design real-life "
"objects of any size. <a href=\"http://en.wikipedia.org/wiki/"
"Parametric_feature_based_modeler\">Parametric modeling</a> allows you to "
"easily modify your design by going back into your model history and changing "
"its parameters. FreeCAD is open-source and highly customizable, scriptable "
"and extensible."
msgstr ""
"FreeCAD为一个参数化3D设计软件，主要用于设计任意尺寸的现实生活物品。<a href="
"\"http://en.wikipedia.org/wiki/Parametric_feature_based_modeler\">参数化建模"
"</a>允许您轻松的通过建模历史来修改您的设计及参数。FreeCAD开放源代码，具有高度"
"可定制化、可使用脚本语言和可扩充功能的特点。"

#: index.php:143
msgid ""
"FreeCAD is multiplatfom (Windows, Mac and Linux), and reads and writes many "
"open file formats such as STEP, IGES, STL, SVG, DXF, OBJ, IFC, DAE and many "
"others."
msgstr ""
"FreeCAD为多平台软件(Windows, Mac 及 Linux)，并且可读取及储存多种开放格式文"
"件，例如STEP、 IGES、STL、SVG、DXF、OBJ、IFC、DAE 及更多其他文件格式。"

#: index.php:146
msgid "Read more..."
msgstr "阅读更多..."

#: index.php:149
msgid "Who is FreeCAD for? A couple of user cases:"
msgstr "FreeCAD适用于哪些用户？以下为一些用户案例："

#: index.php:152
msgid ""
"The <b>home user/hobbyist</b>. Got yourself a project you want to build, "
"have built, or 3D printed? Model it in FreeCAD. No previous CAD experience "
"required. Our community will help you get the hang of it quickly!"
msgstr ""
"<b>家庭使用者／业余爱好者</b>。建立您自己想要的项目、已经建立好的项目或是以3D"
"打印印出？请于FreeCAD中建立它，您不需要有CAD的使用经验，因为我们的社群会协助"
"您快速的学会它！"

#: index.php:155
#, fuzzy
#| msgid ""
#| "The <b>experienced CAD user</b>. If you use commercial CAD or BIM "
#| "modeling software at work, you will find similar tools and workflow among "
#| "the many <a href=\"wiki/?title=Workbenches\">workbenches</a> of FreeCAD."
msgid ""
"The <b>experienced CAD user</b>. If you use commercial CAD or BIM modeling "
"software at work, you will find similar tools and workflow among the many <a "
"href=\"wiki/Workbenches\">workbenches</a> of FreeCAD."
msgstr ""
"<b>已有CAD经验的使用者</b>。 若您在工作中使用商业CAD或BIM建模软件，您也可以在"
"FreeCAD中找到许多<a href=\"wiki/?title=Workbenches/zh-tw\">工作台</a>相似的工"
"具及使用流程。"

#: index.php:158
msgid ""
"The <b>programmer</b>. Almost all of FreeCAD's functionality is accessible "
"to <a href=\"https://en.wikipedia.org/wiki/Python_%28programming_language"
"%29\">Python</a>. You can easily extend FreeCAD's functionality, automatize "
"it with scripts, build your own modules or even embed FreeCAD in your own "
"application."
msgstr ""
"<b>程序员</b>。几乎所有FreeCAD的功能都可以由 <a href=\"https://en.wikipedia."
"org/wiki/Python_%28programming_language%29\">Python</a>来使用。 您可以很容易"
"地扩充FreeCAD的功能、以脚本自动化、建立您自己模块或甚至将FreeCAD嵌入您自己的"
"应用程序中。"

#: index.php:161
msgid ""
"The <b>educator</b>. Teach your students a free software with no worry about "
"license purchase. They can install the same version at home and continue "
"using it after leaving school."
msgstr ""
"<b>教育工作者</b>。自由软件可不用担心有关版权购买问题即可对学生教学。而学生亦"
"可以于家中安装相同版本，并且于毕业后持续使用。"

#: index.php:173
msgid "Community"
msgstr "社区"

#: index.php:181
msgid "Learn"
msgstr "学习"

#: index.php:183
msgid "Tutorials"
msgstr "教程"

#: index.php:184
msgid "Youtube videos"
msgstr "Youtube 视频"

#: index.php:189
msgid "Help the project"
msgstr "帮助项目"

#: index.php:191
msgid "How can I help?"
msgstr "如何帮助？"

#: index.php:192
msgid "Donate!"
msgstr "捐赠！"

#: index.php:193
msgid "Translate"
msgstr "翻译"

#: index.php:197
msgid "Code"
msgstr "代码"

#: index.php:199
msgid "Building from source"
msgstr "由源代码安装"

#: index.php:200
msgid "C++ & Python API"
msgstr "C + + & Python API"

#: index.php:201
msgid "License information"
msgstr "许可证信息"

#~ msgid "title=Main_Page"
#~ msgstr "title=Main_Page/zh-cn"

#~ msgid "English"
#~ msgstr "英语"

#~ msgid "French"
#~ msgstr "法语"

#~ msgid "Portuguese (Brazil)"
#~ msgstr "葡萄牙语 (巴西)"

#~ msgid "Spanish"
#~ msgstr "西班牙语"

#~ msgid "title=About_FreeCAD"
#~ msgstr "title=About_FreeCAD/zh-cn"

#~ msgid "title=Feature_list"
#~ msgstr "title=Feature_list/zh-cn"

#~ msgid "title=Screenshots"
#~ msgstr "title=Screenshots/zh-cn"

#~ msgid "title=Download"
#~ msgstr "title=Download/zh-cn"

#~ msgid "title=Getting_started"
#~ msgstr "title=Getting_started/zh-cn"

#~ msgid "title=Tutorials"
#~ msgstr "title=Tutorials/zh-cn"

#~ msgid "title=Donate"
#~ msgstr "title=Donate/zh-cn"

#~ msgid "title=Compiling"
#~ msgstr "title=Compiling/zh-cn"

#~ msgid "title=Licence"
#~ msgstr "title=Licence/zh_cn"
